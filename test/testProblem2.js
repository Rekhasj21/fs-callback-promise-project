const { readFile, unlink, appendFile, writeFile } = require('../problem2')

let sentences
readFile('fsCallbacksPromise/lipusum.txt')
    .then((fileContent) => {
        const upperContent = fileContent.toUpperCase();
        const upperFileName = 'fsCallbacksPromise/upper.txt';

        return writeFile(upperFileName, upperContent)
            .then(() => {
                return appendFile('fsCallbacksPromise/filenames.txt', `${upperFileName}\n`);
            })
            .then(() => {
                const lowerContent = upperContent.toLowerCase();
                sentences = lowerContent.split(/[.!?]/);
                const lowerFileName = 'fsCallbacksPromise/lower.txt';

                return writeFile(lowerFileName, sentences.join('\n'))
                    .then(() => {
                        return appendFile('fsCallbacksPromise/filenames.txt', `${lowerFileName}\n`);
                    });
            })
            .then(() => {
                const sortedContent = sentences.sort().join('\n');
                const sortedFileName = 'fsCallbacksPromise/sorted.txt';

                return writeFile(sortedFileName, sortedContent)
                    .then(() => {
                        return appendFile('fsCallbacksPromise/filenames.txt', `${sortedFileName}\n`);
                    });
            })
            .then(() => {
                return readFile('fsCallbacksPromise/filenames.txt')
                    .then((nameOfFiles) => {
                        const fileNames = nameOfFiles.trim().split('\n');
                        const unlinkPromises = fileNames.map(fileName => {
                            return unlink(fileName).then(() => {
                                console.log(`Deleted ${fileName}`);
                            });
                        });
                        return Promise.all(unlinkPromises);
                    });
            })
            .catch((err) => {
                console.error(`Error: ${err}`);
            });
    })

