const fs = require('fs')
const path = require('path')
const { createRandomJsonFiles, deleteRandomJsonFiles } = require('../problem1')

const numOfFiles = 10;
const dirName = 'jsonFiles';
const dirPath = path.join("fsCallbacksPromise", dirName)


fs.mkdir(dirPath, err => {
    if (err) {
        console.log(`Error in creating directory:${err}`)
        return
    }
    console.log(`Created directory ${dirName}`);
    let promise = Promise.resolve()
    for (let i = 0; i <= numOfFiles; i++) {
        promise = promise.then(() => {
            return createRandomJsonFiles(dirPath)
                .then(() => {
                    console.log(`Created file ${i + 1}`)
                })
                .catch((err) => {
                    console.log(`Error creating file ${err}`)
                })
        })
    }
    promise.then(() => {
        return deleteRandomJsonFiles(dirPath)
    })
        .then(() => {
            console.log(`Deleted files from ${dirName} directory`)
        })
        .catch(err => {
            console.log(`Error: ${err}`)
        })
})
