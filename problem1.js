const fs = require('fs');
const path = require('path');

const numOfFiles = 10;
const dirName = 'jsonFiles';
const dirPath = path.join("fsCallbacksPromise", dirName)

function createRandomJsonFiles() {
  const fileName = `file${Math.ceil(Math.random() * 100)}.json`
  const filePath = path.join(dirPath, fileName)
  const content = JSON.stringify({ fileName })

  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, content, (err) => {
      if (err) {
        reject(err)
      }
      else {
        resolve()
      }
    })
  })
}

function deleteRandomJsonFiles(dir) {
  fs.readdir(dir, (err, files) => {
    if (err) {
      return Promise.reject(err)
    }
    const promise = files.map(file => {
      const dirFilePath = path.join(dir, file);
      return new Promise((resolve, reject) => {
        fs.stat(dirFilePath, (err, status) => {
          if (err) {
            reject(err)
          }
          else {
            fs.unlink(dirFilePath, err => {
              if (err) {
                reject(err)
              } else {
                resolve()
              }
            })
          }
        })
      })
    })
  })
}

module.exports = { createRandomJsonFiles, deleteRandomJsonFiles }